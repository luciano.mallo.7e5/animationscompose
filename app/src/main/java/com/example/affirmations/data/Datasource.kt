/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.data

import com.example.affirmations.R
import com.example.affirmations.model.Affirmation

/**
 * [Datasource] generates a list of [Affirmation]
 */
class Datasource() {
    fun loadAffirmations(): List<Affirmation> {
        return listOf<Affirmation>(
            Affirmation(1, R.string.affirmation1, R.drawable.image1, R.string.Description1,R.string.Telephone1,R.string.Email1),
            Affirmation(2, R.string.affirmation2, R.drawable.image2, R.string.Description2,R.string.Telephone2,R.string.Email2),
            Affirmation(3, R.string.affirmation3, R.drawable.image3, R.string.Description3,R.string.Telephone3,R.string.Email3),
            Affirmation(3, R.string.affirmation4, R.drawable.image4, R.string.Description4,R.string.Telephone4,R.string.Email4),
            Affirmation(4, R.string.affirmation5, R.drawable.image5, R.string.Description5,R.string.Telephone5,R.string.Email5),
            Affirmation(6, R.string.affirmation6, R.drawable.image6, R.string.Description6,R.string.Telephone6,R.string.Email6),
            Affirmation(7, R.string.affirmation7, R.drawable.image7, R.string.Description7,R.string.Telephone7,R.string.Email7),
            Affirmation(8, R.string.affirmation8, R.drawable.image8, R.string.Description8,R.string.Telephone8,R.string.Email8),
            Affirmation(9, R.string.affirmation9, R.drawable.image9, R.string.Description9,R.string.Telephone1,R.string.Email9),
            Affirmation(10, R.string.affirmation10, R.drawable.image10, R.string.Description10,R.string.Telephone10,R.string.Email10)
        )
    }
}
