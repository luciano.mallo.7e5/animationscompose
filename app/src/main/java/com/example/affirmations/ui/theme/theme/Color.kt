/*
 * Copyright (C) 2021 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.affirmations.ui.theme

import androidx.compose.ui.graphics.Color

val DarkGreen = Color(0xFF4F9400)
val LightGreen = Color(0xFF63B603)
val Blue = Color(0xFF003E75)
val Violet = Color(0xFFC700FF)
val Red=Color(0xFFFF5252)
val DarkRed= Color(0xFF970000)
val White =Color(0xFFFFFFFF)
val DarkViolet = Color(0xFF650081)
