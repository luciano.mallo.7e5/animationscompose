import com.example.affirmations.R
import com.example.affirmations.model.Affirmation
import com.example.affirmations.ui.theme.viewmodel.DetailViewModel
import junit.framework.TestCase.assertEquals
import org.junit.Assert.assertNotEquals
import org.junit.Test


class DetailViewModelTest {
    private val viewModel = DetailViewModel()

    val affirmation = Affirmation(
        1, R.string.affirmation1, R.drawable.image1, R.string.Description1,
        R.string.Telephone1,
        R.string.Email1
    )

    @Test
    fun viewModel_GetCorrectAffirmationByTheID() {
        viewModel.getAffirmation(1)
        val currentGameUiState = viewModel.uiState.value
        assertEquals( affirmation.stringDescriptionId,currentGameUiState!!.stringDescriptionId)
    }
    @Test
    fun viewModel_GetInCorrectAffirmationByTheID() {
        viewModel.getAffirmation(2)
        val currentGameUiState = viewModel.uiState.value
        assertNotEquals( affirmation.stringDescriptionId,currentGameUiState!!.stringDescriptionId)
    }


}