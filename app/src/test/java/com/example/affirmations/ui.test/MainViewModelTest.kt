package com.example.affirmations.ui.test


import com.example.affirmations.data.Datasource
import com.example.affirmations.ui.theme.viewmodel.MainViewModel
import junit.framework.TestCase.assertEquals
import org.junit.Test

class MainViewModelTest {

    private val viewModel = MainViewModel()
    val affirmations = Datasource().loadAffirmations()

    @Test
    fun viewModel_GetCorrectAffirmationByTheID() {
        val currentGameUiState = viewModel.uiState.value
        assertEquals(affirmations.size, currentGameUiState.size)
    }

}